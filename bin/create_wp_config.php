#!/usr/bin/php

<?php
/*

 * This is a quick script to set up the correct wp-config file.
 * Done in PHP as we have it, and the debian-slim image doesn't have curl or
 * wget.  Which is probably a good thing.

 * But I know we do have PHP with curl support...
 */

$HEAD = <<<'EOF'
<?php

/* Database settings via env vars, for 12factor niceness. */
define( 'DB_NAME', getenv('DATABASE_DATABASE') ?: 'wp' );
define( 'DB_USER', getenv('DATABASE_USER') ?: 'wp' );
define( 'DB_PASSWORD', getenv('DATABASE_PASSWORD') );
define( 'DB_HOST', getenv('DATABASE_HOST') ?: 'mysql' );
define( 'DB_CHARSET', getenv('DATABASE_CHARSET') ?: 'utf8mb4' );
define( 'DB_COLLATE', getenv('DATABASE_COLLATE') ?: '' );

// The following keys generated by the wordpress API:

EOF;

$FOOT = <<<'EOF'

$table_prefix = getenv('WP_TABLE_PREFIX') ?: 'wp_';

/* Change this if you need to: */
define ('WP_DEBUG', getenv('WP_DEBUG') === 'true' ? true : false);

/* We are on HTTPS. Forever */
$_SERVER['HTTPS']='on';

/* File editing is a good way to break things */
define('DISALLOW_FILE_EDIT', true);

/* We do cron (scheduled tasks) via WP CLI in the background */
define('DISABLE_WP_CRON', true);

/* Where is wordpress? */
if ( ! defined('ABSPATH') ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/* Include other WP settings stuff... */
require_once ABSPATH . 'wp-settings.php';

EOF;


function get_secrets($fp) {
	// Get some new secrets from the wordpress secrets builder API:
	$ch = curl_init('https://api.wordpress.org/secret-key/1.1/salt/');
	curl_setopt($ch, CURLOPT_FILE, $fp);
	curl_setopt($ch, CURLOPT_HEADER, 0);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
	curl_exec($ch);
	if(curl_error($ch)) {
		echo "Oh no - failed to download new secrets from api.wordpress.org";
		exit(1);
	}
	curl_close($ch);
}

$fp = fopen('wp-config.php', 'w');
fwrite($fp, $HEAD);
get_secrets($fp);
fwrite($fp, $FOOT);
fclose($fp);
?>

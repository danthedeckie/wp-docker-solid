#!/bin/sh
# If no wp-settings found, download wordpress.
# If no wp-config found, generate one (that takes env vars)

WPDIR=/var/www

if [ ! -f "$WPDIR/wp-settings.php" ]; then
    cd $WPDIR
    su -m php-owner -c '/usr/local/bin/wp-cli core download'
fi

if [ ! -f "$WPDIR/wp-config.php" ]; then
    cd $WPDIR
    su -m php-owner -c '/usr/local/bin/create_wp_config.php'
    chmod 640 "$WPDIR/wp-config.php"
fi

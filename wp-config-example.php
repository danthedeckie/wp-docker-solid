<?php

/* Database settings via env vars, for 12factor niceness. */
define( 'DB_NAME', getenv('DATABASE_DATABASE') ?: 'wp' );
define( 'DB_USER', getenv('DATABASE_DATABASE') ?: 'wp' );
define( 'DB_PASSWORD', getenv('DATABASE_DATABASE') );
define( 'DB_HOST', getenv('DATABASE_DATABASE') ?: 'mysql' );
define( 'DB_CHARSET', getenv('DATABASE_CHARSET') ?: 'utf8mb4' );
define( 'DB_COLLATE', getenv('DATABASE_CHARSET') ?: '' );

/* Apparently these can be changed without too much bother? */
define('AUTH_KEY', getenv('AUTH_KEY') );
define('SECURE_AUTH_KEY', getenv('SECURE_AUTH_KEY'));
define('LOGGED_IN_KEY', getenv('LOGGED_IN_KEY'));
define('NONCE_KEY', getenv('NONCE_KEY'));
define('AUTH_SALT', getenv('AUTH_SALT'));
define('SECURE_AUTH_SALT', getenv('SECURE_AUTH_SALT'));
define('LOGGED_IN_SALT', getenv('LOGGED_IN_SALT'));
define('NONCE_SALT', getenv('NONCE_SALT'));

$table_prefix = getenv('WP_TABLE_PREFIX') ?: 'wp_';

/* Change this if you need to: */
define ('WP_DEBUG', false);

/* We are on HTTPS. Forever */
$_SERVER['HTTPS']='on';

/* File editing is a good way to break things */
define('DISALLOW_FILE_EDIT', true);

/* We do cron (scheduled tasks) via WP CLI in the background */
define('DISABLE_WP_CRON', true);

/* Where is wordpress? */
if ( ! defined('ABSPATH') ) {
    define( 'ABSPATH', __DIR__ . '/' );
}

/* Include other WP settings stuff... */
require_once ABSPATH . 'wp-settings.php';

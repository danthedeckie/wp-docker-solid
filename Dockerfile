FROM debian:buster
# TODO - try alpine variation
#        note - the nginx cache purging stuff is included only in nginx-extras version on debian...

# S6 multi-processes-in-1-container-overlay:
ADD https://github.com/just-containers/s6-overlay/releases/download/v2.2.0.1/s6-overlay-amd64.tar.gz /tmp/


# Extract the scripts to the root of the container
RUN tar xzf /tmp/s6-overlay-amd64.tar.gz -C /

# Cleanup!
RUN rm /tmp/s6-overlay-amd64.tar.gz


# Install nginx, PHP, cron, and cleanup apt db
RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y nullmailer cron nginx-extras \
	 less vsftpd \
         php7.3-fpm php7.3-cli php7.3-mysql php7.3-gd php7.3-enchant php7.3-mbstring \
	 php7.3-curl php7.3-xml php7.3-readline php7.3-bz2 php7.3-zip php7.3-dom php7.3-imagick && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Turn on options for PHP:
RUN echo 'short_open_tag = On' >> /etc/php/7.3/fpm/php.ini 
RUN mkdir /run/php
# default chroot for FTP server:
RUN mkdir /tmp/ftp

# Create appropriate users:
RUN addgroup --gid 2000 php && \
    useradd php-serve --uid 2000 -g php -s /usr/sbin/nologin --no-create-home --no-user-group && \
    useradd php-owner --uid 2001 -g php --no-create-home -d /var/www --no-user-group

# add cron:
COPY conf/crontab /etc/cron.d/wordpress
COPY bin/* /usr/local/bin/
RUN chmod 0600 /etc/cron.d/wordpress

# turn on services:
COPY ./conf/services.d /etc/services.d

# Add extra init scripts:
COPY ./conf/cont-init.d /etc/cont-init.d

# WP CLI:
ADD https://raw.githubusercontent.com/wp-cli/builds/gh-pages/phar/wp-cli.phar /usr/local/bin/wp-cli

# Load in configuration (last step, as it's more docker-build-cacheable)
COPY conf/vsftpd.conf /etc/vsftpd.conf
COPY conf/php-fpm-www.conf /etc/php/7.3/fpm/pool.d/www.conf
COPY conf/nginx-site.conf /etc/nginx/sites-available/default
COPY conf/php-permissions /etc/fix-attrs.d/php-permissions

VOLUME ["/tmp/fastcgi_cache", "/var/www, /var/spool/nullmailer"]

EXPOSE 80

ENTRYPOINT ["/init"]

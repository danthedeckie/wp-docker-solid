# WP-Docker-Solid

WORK-IN-PROGRESS

One docker image to run a stand-alone wordpress install that looks after itself
and has decent security.

## A Wordpress-in-Docker image designed for security.
- WP-CLI for frequent updates
- Separation of PHP-owner, PHP-runner and NGiNX users
- Plugins / themes / updates must be installed manually by the user (in the
  wordpress interface) requireing an extra password - but misbehaving plugins
  cannot modify the wordpress install directly.

## Things that are kinda interesting:
- nullmailer is there - but not running - so emails sent will queue up
- a cron job frequently cats queued emails to the docker stdout log, and
  archives them
- The extra level of security (not letting PHP write new .php files, etc) comes
  from wordpress support for install-via-ftp - so there's an internal
  localhost-only FTP server with a single user & separate password, which the
  user must manually enter if they want to install plugins themselves.

## System based on:
- debian (buster)
- s6 init (multiprocess in docker)
- wp-cli
- php-fpm (7.3)
- nginx
- vsftp (localhost only, wordpress uses it to do plugin & theme updates without
  letting the running PHP user have write permission to PHP code)
- nullmailer (can be configured to send mail to external SMTP, or not.

# Permissions / Users:

- nginx (can read only the files it needs to serve)
- php-owner (read-write permission of the whole /var/www)
- php-serve (can read-only /var/www, and write in wp-content only)

php-owner runs wp-cli to do updates every few hours in cron.

When a logged in wordpress user wants to install new plugins / themes,
wordpress shows them a FTP form, they must manually enter a the username and
password for php-owner on localhost.

Every few hours the permissions are re-inforced by a cron job.

# Configuration ENV Vars:

- `DATABASE_HOST`
- `DATABASE_USER`
- `DATABASE_NAME`
- `DATABASE_PASSWORD`
- `MAIL_URL` (TODO?)

# Volumes:

- `/var/www/` - what content actually gets served.
- `/tmp/fastcgi_cache/` - cached content by nginx (currently disabled)

# Secrets:
- `userpassword` (Note: If you can't use secrets directly (old docker, no
  swarm, etc) you can fake it by adding a volume:
- `/run/secrets/userpassword`

The userpassword should be secret, but it being stored in plaintext on the host
isn't the end of the world - all it allows the user to do is connect via FTP
and write to the directory - which regular wordpress allows without any kind of
password anyway.

**TODO** - there should be an option to store this encrypted - it's easy enough to
do, just change chpasswd to use already encrypted (w/ -e) or something.
